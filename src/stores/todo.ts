import { defineStore } from 'pinia';
import { Todo } from '@/interfaces/todo';

interface State {
  todos: Todo[];
}

export const useTodoStore = defineStore('todo', {
  state: (): State => ({
    todos: [],
  }),
  actions: {
    addTodo(todo: Todo) {
      this.todos.push(todo);
    },
  },
});
